Initial approche

Here I’m using two Cloudformation stacks for ECR creation and VPC,ECS to gather in a single stack
The initial plan was to create the ECR with a dummy image and then to create the VPC and ECS cluster with zero task running, as a fresh infrastructure. 
And on the Build step to build the image and push the image to ECR repository
And on the deploy step to deploy only the task definition with Desired task with the latest image version from ECR.

The reason for this approach was to isolate provisioning infrastructure and CI/CD, to minimize overall performance of build time.
In order to provision infrastructure separately and build and deploy separately we have to use 2 repositories. One for infrastructure as a code and other for code repository.
Since I’m supposed to produce a pipeline in single repository, I was not able to achieve this approach. Instead, I have created this on a single pipeline which is not the best approach though.

I created this pipeline using a cooperate account due to the build time limitation on a free account. So I want be able to share the repo which I created this pipeline, but I have created a free account which I’m sharing now and committed the files related to the pipeline.

Deployment Instruction:

There is a list of environment variables for this pipeline, please change the values according to your environment. And Run the pipeline. By default, Infrastructure creation and build and deploy are triggered automatically. Refresh and Destroy steps are triggered manually.
Once the deployment is done, you may check the app from the loadbalancer DNS
Note: I have not added SSL for loadbalancer
